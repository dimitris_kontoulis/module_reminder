<?php

use Illuminate\Database\Seeder;
use App\Models\Module;

class ThirdpartyDevSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 7; $i++){
            Module::insert([
                [
                    'course_key' => 'xfz',
                    'name' => 'XFZ Module ' . $i
                ],

                [
                    'course_key' => 'xyz',
                    'name' => 'XYZ Module ' . $i
                ],

                [
                    'course_key' => 'xzz',
                    'name' => 'XZZ Module ' . $i
                ]
            ]);
        }


    }
}
