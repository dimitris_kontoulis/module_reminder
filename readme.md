## About Module Reminder

This project was created after a request form a third-party Company.
This is a part of production code, but stripped down to some of its functionality, while also excluding the third-party from it.
There was a laravel package from the company with some functionality for communication with their system. All occurrences were replaced with `Thirdparty`.
The code is not functional because of the thirdparty part missing, but tests are running since I am mocking the thirdparty service.

### Description
A customer of the `thirdparty` purchases courses.
Each course (xyz, xfz, xzz) consists of 7 video modules.
We need an endpoint that takes an email of a `thirdparty` user and returns a reminder for the next module the user should watch. 
The order of the modules comes from the order the courses were purchased. 

####Decision logic.

If no modules are completed it should attach first tag in order. 
In case any of first course modules are completed then it should attach next uncompleted module after the last completed of the first course. (e.g.. M1, M2 & M4 are completed, then attach M5 tag). 
If all (or last) first course modules are completed - attach next uncompleted module after the last completed of the second course. Same applies in case of a third course. 
If all (or last) modules of all courses are completed - attach “Module reminders completed” tag.

#### Development
- Repository pattern is used for DRY and testing purposes
- Swagger API doc at `/api/docs`
- Tests are using sqlite
