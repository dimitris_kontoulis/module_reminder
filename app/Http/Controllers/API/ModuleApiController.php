<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ThirdpartyHelper;
use App\Http\Requests\ModuleReminderRequest;
use App\Models\Tag;
use App\Repositories\ModuleRepository;
use App\Repositories\UserRepository;


/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Third Party API",
 *     version="1.0.0",
 *   )
 * )
 */

class ModuleApiController extends Controller
{

    /**
     * @var ModuleRepository
     */
    private $moduleRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(ModuleRepository $moduleRepository, UserRepository $userRepository){
        $this->moduleRepository = $moduleRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param ModuleReminderRequest $request
     * @param ThirdpartyHelper $thirdparty
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *      path="/module_reminder_assigner",
     *      summary="Set next reminder tag for user.",
     *      description="Sets next module reminder tag, based on user bought courses and modules completed",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          type="string",
     *          description="Customer email to set next module reminder",
     *          required=true
     *      ),
     *      tags={"Module"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          ),
     *     examples={"success":"true", "message":"Module reminders completed"}
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="failed operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          ),
     *          examples={"success":"false", "message":"No modules found for order"}
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="Unprocessable entity",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="errors",
     *                  type="array",
     *                  @SWG\Items(
     *
     *                  )
     *              )
     *          ),
     *          examples={"message":"The given data was invalid.", "errors":{"email":"The email field is required."}}
     *      )
     * )
     */
    public function moduleReminderAssigner(ModuleReminderRequest $request, ThirdpartyHelper $thirdparty)
    {
        $success = true;
        try {
            $contact = $thirdparty->getContact($request->email);
            if (!$contact) {
                throw(new \Exception("Contact for " . $request->email . " not found."));
            }
            if (empty($contact["_Products"])) {
                throw(new \Exception("Contact has empty order."));
            }
            $user = $this->userRepository->findByField("email", $request->email)->first();
            $modules = $this->moduleRepository->getModulesFromOrder($contact["_Products"]);
            if ($modules->count() === 0) {
                throw(new \Exception("No modules found for order " . $contact["_Products"]));
            }
            $nextModule = $this->moduleRepository->getNextModule($user, $modules);
            if (is_null($nextModule) && $user->completedModules->count()) {
                /**
                 * user has completed all modules
                 */
                $tag = Tag::where("name", "Module reminders completed")->firstOrFail();
            } else {
                $tag = $nextModule->getReminderTag();
            }

            $result = $thirdparty->addTag($contact["Id"], $tag->id);
            if ($result === false) {
                throw(new \Exception("Could not add tag $tag->name to $request->email "));
            }
            $message = $tag->name;
        } catch (\Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }

        return response()->json(compact("success", "message"), ($success ? 200 : 400));
    }

}
