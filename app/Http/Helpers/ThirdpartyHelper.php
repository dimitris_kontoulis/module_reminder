<?php

namespace App\Http\Helpers;

use Thirdparty;
use Log;
use Storage;
use Request;


class ThirdpartyHelper
{

    public function __construct()
    {
        if (Storage::exists('inf_token')) {

            Thirdparty::setToken(unserialize(Storage::get("inf_token")));

        } else {
            Log::error("Thirdparty token not set.");
        }
    }

    public function authorize(){
        if (Request::has('code')) {
            Thirdparty::requestAccessToken(Request::get('code'));

            Storage::put('inf_token', serialize(Thirdparty::getToken()));
            Log::notice('Thirdparty token created');

            Thirdparty::setToken(unserialize(Storage::get("inf_token")));

            return 'Success';
        }

        return '<a href="' . Thirdparty::getAuthorizationUrl() . '">Authorize Thirdparty</a>';
    }

    public function getAllTags(){
        try {

            return Thirdparty::tags()->all();

        } catch (\Exception $e){
            Log::error((string) $e);
            return false;
        }
    }

    public function getContact($email)
    {

        $fields = [
            'Id',
            'Email',
            'Groups',
            "_Products"
        ];

        try {

            return Thirdparty::contacts('xml')->findByEmail($email, $fields)[0];

        } catch (\Exception $e){
            Log::error((string) $e);
            return false;
        }
    }

    public function addTag($contact_id, $tag_id){
        try {
            return Thirdparty::contacts('xml')->addToGroup($contact_id, $tag_id);

        } catch (\Exception $e){
            Log::error((string) $e);
            return false;
        }
    }

    public function createContact($data){

        try {
            return Thirdparty::contacts('xml')->add($data);

        } catch (\Exception $e){
            Log::error((string) $e);
            return false;
        }
    }


}
