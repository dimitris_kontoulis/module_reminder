<?php


namespace App\Repositories;

use App\Models\Module;
use App\User;

/**
 * Class ModuleRepository
 * @package App\Repositories
 */

class ModuleRepository extends BaseRepository
{

    public function model()
    {
        return Module::class;
    }

    /**
     * Make a module collection from the string we receive from the Thirdparty API
     * @param $order
     * @return \Illuminate\Support\Collection
     */
    public function getModulesFromOrder($order)
    {
        $productArray = explode(",", $order);
        $modules = collect();
        if (strpos($productArray[0], " ") !== false) {
            $course = explode(" ", $productArray[0])[0];
            $productArray[0] = ltrim($productArray[0], $course . " ");
            foreach ($productArray as $module) {
                $modules[] = Module::where("name", "like", $course . " Module " . ltrim($module, "M"))->first();
            }
        } else {
            foreach ($productArray as $product) {
                $modules = $modules->merge(Module::where('course_key', $product)->orderBy("name")->get());
            }
        }
        return $modules;
    }

    /**
     * @param User $user
     * @param $modules
     * @return Module|null
     */
    public function getNextModule(User $user, $modules)
    {
        foreach ($modules->groupBy("course_key") as $modules) {
            $lastCompletedModule = $user->completedModules()
                ->whereIn("module_id", $modules->pluck("id"))
                ->orderBy("module_id", "desc")
                ->first();

            if (is_null($lastCompletedModule)) {
                return $modules->first();
            }
            if ($lastCompletedModule->id != $modules->last()->id) {
                return $modules->firstWhere("name", ">", $lastCompletedModule->name);
            }
        }
    }
}