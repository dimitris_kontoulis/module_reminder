<?php


namespace App\Repositories;
use App\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */

class UserRepository extends BaseRepository
{

    public function model()
    {
        return User::class;
    }

}