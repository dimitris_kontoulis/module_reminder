<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Http\Helpers\ThirdpartyHelper;
use App\Models\Tag;
use Illuminate\Console\Command;

class ImportTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports tags from Thirdparty to database';

    /**
     * Keep category ids we checked for existence
     * @var array
     */
    private $checkedCategories = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!app()->runningUnitTests()) {
            $thirdpartyHelper = new ThirdpartyHelper();
            $tags = $thirdpartyHelper->getAllTags()->toArray();

            // Store them for testing purposes
            file_put_contents(storage_path("tags.json"), json_encode($tags));
        } else {
            // Load from file for testing
            $tags = json_decode(file_get_contents(storage_path("tags.json")));
            // Change category to array
            $tags = array_map(function ($data) {
                $data->category = json_decode(json_encode($data->category), true);
                return $data;
            }, $tags);
        }
        foreach ($tags as $tag) {
            if (!empty($tag->category)) {
                $this->makeCategory($tag->category);
            }
            Tag::updateOrCreate(["id" => $tag->id], [
                "name" => $tag->name,
                "description" => $tag->description,
                "category_id" => $tag->category["id"]
            ]);
        }
    }

    /**
     * Create a category if it doesn't exist
     * @param $category
     */
    private function makeCategory($category)
    {
        if (!array_key_exists($category["id"], $this->checkedCategories)) {
            Category::updateOrCreate(["id" => $category["id"]], [
                "name" => $category["name"],
                "description" => $category["description"]
            ]);
            $this->checkedCategories[$category["id"]] = $category["id"];
        }
    }
}
