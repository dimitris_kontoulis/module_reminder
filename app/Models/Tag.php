<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        "id",
        "name",
        "description",
        "category_id"
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
