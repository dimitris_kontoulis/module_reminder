<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function getReminderTag()
    {
        return Tag::where("name", "like", "Start ". $this->name." Reminders")->first();
    }
}
