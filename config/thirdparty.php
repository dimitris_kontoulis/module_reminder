<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Your Thirdparty OAuth2 Credentials
	|--------------------------------------------------------------------------
	*/

	'clientId' => env('THIRDPARTY_CLIENT_ID'),

	'clientSecret' => env('THIRDPARTY_SECRET'),

	'redirectUri' => env('THIRDPARTY_REDIRECT_URL'),

);
