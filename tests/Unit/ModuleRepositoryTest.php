<?php

namespace Tests\Unit;

use App\Repositories\ModuleRepository;
use Tests\TestCase;

class ModuleRepositoryTest extends TestCase
{
    private $moduleRepository;

    protected function setUp(): void
    {
        parent::setUp();

        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        \Artisan::call('import:tags');
        $this->moduleRepository = $this->app->make(ModuleRepository::class);
        $this->withoutExceptionHandling();
    }

    public function test_single_course_module_creation()
    {
        $order = "XFZ M1,M2,M3,M4,M5,M6,M7";
        $modules = $this->moduleRepository->getModulesFromOrder($order)->pluck("name")->toArray();
        $this->assertEquals($modules, [
            "XFZ Module 1",
            "XFZ Module 2",
            "XFZ Module 3",
            "XFZ Module 4",
            "XFZ Module 5",
            "XFZ Module 6",
            "XFZ Module 7",
        ]);
    }

    public function test_multi_course_module_creation()
    {
        $order = "xzz,xfz";
        $modules = $this->moduleRepository->getModulesFromOrder($order)->pluck("name")->toArray();
        $this->assertEquals($modules, [
            "XZZ Module 1",
            "XZZ Module 2",
            "XZZ Module 3",
            "XZZ Module 4",
            "XZZ Module 5",
            "XZZ Module 6",
            "XZZ Module 7",
            "XFZ Module 1",
            "XFZ Module 2",
            "XFZ Module 3",
            "XFZ Module 4",
            "XFZ Module 5",
            "XFZ Module 6",
            "XFZ Module 7",
        ]);
    }

    public function test_multi_course_module_creation_wrong_ordering()
    {
        $order = "xfz,xzz";
        $modules = $this->moduleRepository->getModulesFromOrder($order)->pluck("name")->toArray();
        $this->assertNotEquals($modules, [
            "XZZ Module 1",
            "XZZ Module 2",
            "XZZ Module 3",
            "XZZ Module 4",
            "XZZ Module 5",
            "XZZ Module 6",
            "XZZ Module 7",
            "XFZ Module 1",
            "XFZ Module 2",
            "XFZ Module 3",
            "XFZ Module 4",
            "XFZ Module 5",
            "XFZ Module 6",
            "XFZ Module 7",
        ]);
    }

    public function test_next_module_single_course_none_completed()
    {
        $user = factory('App\User')->create();
        $order = "XZZ M1,M2,M3,M4,M5,M6,M7";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertEquals("XZZ Module 1", $nextModule->name);
    }

    public function test_next_module_single_course_last_completed()
    {
        $user = factory('App\User')->create();
        $order = "XZZ M1,M2,M3,M4,M5,M6,M7";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $user->completedModules()->attach($modules->last()->id);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertNull($nextModule);
    }

    public function test_next_module_single_course_some_completed()
    {
        $user = factory('App\User')->create();
        $order = "XYZ M1,M2,M3,M4,M5,M6,M7";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $user->completedModules()->attach([$modules->get(2)->id, $modules->get(4)->id]);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertEquals("XYZ Module 6", $nextModule->name);
    }

    public function test_next_module_multi_course_none_completed()
    {
        $user = factory('App\User')->create();
        $order = "xfz,xzz";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertEquals("XFZ Module 1", $nextModule->name);
    }

    public function test_next_module_multi_course_all_last_completed()
    {
        $user = factory('App\User')->create();
        $order = "xfz,xzz";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $user->completedModules()->attach([$modules->get(6)->id, $modules->get(13)->id]);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertNull($nextModule);
    }

    public function test_next_module_multi_course_first_last_completed()
    {
        $user = factory('App\User')->create();
        $order = "xfz,xzz";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $user->completedModules()->attach([$modules->get(6)->id, $modules->get(10)->id]);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertEquals("XZZ Module 5", $nextModule->name);
    }

    public function test_next_module_multi_course_last_last_completed()
    {
        $user = factory('App\User')->create();
        $order = "xfz,xzz";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $user->completedModules()->attach([$modules->get(2)->id, $modules->get(13)->id]);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertEquals("XFZ Module 4", $nextModule->name);
    }

    public function test_next_module_multi_course_some_completed()
    {
        $user = factory('App\User')->create();
        $order = "xfz,xzz";
        $modules = $this->moduleRepository->getModulesFromOrder($order);
        $user->completedModules()->attach([$modules->get(3)->id, $modules->get(9)->id]);
        $nextModule = $this->moduleRepository->getNextModule($user, $modules);
        $this->assertEquals("XFZ Module 5", $nextModule->name);
    }
}
