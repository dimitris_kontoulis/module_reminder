<?php

namespace Tests\Feature;

use App\Http\Helpers\ThirdpartyHelper;
use App\Repositories\ModuleRepository;
use Mockery;
use Tests\TestCase;

class ModuleApiTest extends TestCase
{

    private $users;
    /**
     * @var \Mockery\MockInterface
     */
    private $thirdpartyMock;
    private $moduleRepository;

    protected function setUp(): void
    {
        parent::setUp();

        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        \Artisan::call('import:tags');

        $this->withoutExceptionHandling();
        $this->moduleRepository = $this->app->make(ModuleRepository::class);
        $this->thirdpartyMock = Mockery::mock(ThirdpartyHelper::class);
        $this->thirdpartyMock->shouldReceive('addTag')->andReturn(true);
        $this->app->instance(ThirdpartyHelper::class, $this->thirdpartyMock);
        $this->users = factory('App\User', 10)->create();

        foreach ($this->users as $user) {
            $user->order = $this->makeOrders();
        }
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function test_email_not_exist()
    {
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn(null);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $this->users->first()->email]
        );
        $response->assertStatus(400)->assertJson([
            "success" => false,
            "message" => "Contact for " . $this->users->first()->email . " not found."
        ]);
    }

    public function test_empty_order()
    {
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => ""
        ]);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $this->users->first()->email]
        );
        $response->assertStatus(400)->assertJson([
            "success" => false,
            "message" => "Contact has empty order."
        ]);
    }

    public function test_no_modules_found()
    {
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => "test-non-existent"
        ]);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $this->users->first()->email]
        );
        $response->assertStatus(400)->assertJson([
            "success" => false,
            "message" => "No modules found for order test-non-existent"
        ]);
    }

    public function test_no_module_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => $user->order
        ]);
        $modules = $this->moduleRepository->getModulesFromOrder($user->order);
//        dd($modules);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $this->users->first()->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => $modules->first()->getReminderTag()->name
        ]);
    }

    public function test_all_modules_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => $user->order
        ]);
        $modules = $this->moduleRepository->getModulesFromOrder($user->order);
        $user->completedModules()->attach($modules->pluck("id"));
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Module reminders completed"
        ]);
    }

    public function test_all_last_modules_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => $user->order
        ]);
        $modules = $this->moduleRepository->getModulesFromOrder($user->order);
        $user->completedModules()->attach($modules->filter(function ($data) {
            return strpos($data->name, "7") !== false;
        })->pluck("id"));
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Module reminders completed"
        ]);
    }

    public function test_single_course_none_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => "XYZ M1,M2,M3,M4,M5,M6,M7"
        ]);

        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Start XYZ Module 1 Reminders"
        ]);
    }

    public function test_single_course_some_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => "XYZ M1,M2,M3,M4,M5,M6,M7"
        ]);
        $modules = $this->moduleRepository->getModulesFromOrder("XYZ M1,M2,M3,M4,M5,M6,M7");
        $user->completedModules()->attach([$modules->get(2)->id, $modules->get(5)->id]);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Start XYZ Module 7 Reminders"
        ]);
    }

    public function test_multi_course_none_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => "xyz,xzz,xfz"
        ]);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Start XYZ Module 1 Reminders"
        ]);
    }

    public function test_multi_course_some_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => "xyz,xzz,xfz"
        ]);
        $modules = $this->moduleRepository->getModulesFromOrder("xyz,xzz");
        $user->completedModules()->attach([$modules->get(2)->id, $modules->get(10)->id]);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Start XYZ Module 4 Reminders"
        ]);
    }

    public function test_multi_course_last_only_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => "xyz,xzz"
        ]);
        $modules = $this->moduleRepository->getModulesFromOrder("xyz,xzz");
        $user->completedModules()->attach($modules->last()->id);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Start XYZ Module 1 Reminders"
        ]);
    }

    public function test_multi_course_two_last_completed()
    {
        $user = $this->users->random();
        $this->thirdpartyMock->shouldReceive('getContact')->andReturn([
            "Id" => 1234,
            "_Products" => "xfz,xyz,xzz"
        ]);
        $modules = $this->moduleRepository->getModulesFromOrder("xfz,xyz,xzz");
        $user->completedModules()->attach([$modules->get(6)->id, $modules->get(8)->id, $modules->get(19)->id]);
        $response = $this->post(
            '/api/v1/module_reminder_assigner',
            ['email' => $user->email]
        );
        $response->assertStatus(200)->assertJson([
            "success" => true,
            "message" => "Start XYZ Module 3 Reminders"
        ]);
    }

    private function makeOrders()
    {
        $courses = ["xzz", "xfz", "xyz"];
        shuffle($courses);
        $courses = collect($courses)->random(rand(1, count($courses)));

        if ($courses->count() > 1) {
            return $courses->implode(",");
        }
        return strtoupper($courses->first()) . " M1,M2,M3,M4,M5,M6,M7";
    }
}
